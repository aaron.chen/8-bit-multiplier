# 8-bit-multiplier
(implementation of Lab 5 for ECE 385)
### By Aaron Chen, Shruti Chanumolu

8-bit multiplier circuit implemented in SystemVerilog.

In this experiment we designed a multiplier for two 8-bit 2’s complement numbers and implemented it using SystemVerilog on the DE2 FPGA board. We used a simple add shift algorithm to multiply the two numbers which utilised a 9 bit adder/subtractor using full adder primitives that we created.

Multiplier.sv should be set as top level.

<img src="multiplier_block.png" alt="multiplier_block" />
