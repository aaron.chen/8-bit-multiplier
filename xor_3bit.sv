//3-bit XOR
module xor_3bit
(
	input logic a,b,c,
	output logic out
);

assign out = (a & ~b & ~c) | (~a & b & ~c) | (~a & ~b & c) | (a & b & c); //odd no. of highs
	 
endmodule : xor_3bit
